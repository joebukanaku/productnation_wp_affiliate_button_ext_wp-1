<?php
/**
*   {Tag} PN Custom
*   Custom Page for Popup Text Setting
*/

?>

<h1> PN Link Popup Setting </h1>

<div id="pn-link-text-setting">
    <div id="post-body" class="metabox-holder columns-2">
        <!-- Content -->
        <div id="post-body-content">
            <!-- Content -->
    		<div id="post-body-content">
				<div id="normal-sortables" class="meta-box-sortables ui-sortable">
	                <div class="postbox">
	                    <h3 class="hndle"><?php _e( 'Text Settings', $this->plugin_name ); ?></h3>

	                    <div class="inside">
		                    <form action="options-general.php?page=pn-setting" method="post">
                                <p>
		                    		<label for="param_tracker">
                                        <strong><?php //_e( 'Parameter Tracker', $this->plugin_name ); ?></strong>
                                        <small> The parameter we want to track </small>
                                    </label><br>
                                    <input type="text" name="param_tracker" id="param_tracker" class="regular-text" value="<?php echo $this->settings['param_tracker']; ?>">
		                    	</p>
                                <p>
		                    		<label for="pn_link_click_url">
                                        <strong><?php _e( 'Click URL', $this->plugin_name ); ?></strong>
                                        <small> The URL of the click </small>
                                    </label><br>
                                    <input type="text" name="pn_link_click_url" id="pn_link_click_url" class="regular-text" value="<?php echo $this->settings['pn_link_click_url']; ?>">
		                    	</p>
		                    	<p>
		                    		<label for="pn_link_modal_text"><strong><?php _e( 'Popup Display Text', $this->plugin_name ); ?></strong></label><br>
                                    <textarea name="pn_link_modal_text" id="pn_link_modal_text" style="font-size:18px" class="widefat"><?php echo $this->settings['pn_link_modal_text']; ?></textarea>
                                    <!-- <input name="pn_link_text" id="pn_link_text" class="regular-text" /> -->
		                    	</p>
		                    	<?php wp_nonce_field( $this->plugin_name, $this->plugin_name.'_nonce' ); ?>
		                    	<p>
									<input name="submit" type="submit" name="Submit" class="button button-primary" value="<?php _e( 'Save', $this->plugin_name ); ?>" />
								</p>
						    </form>
	                    </div>
	                </div>
	                <!-- /postbox -->
				</div>
				<!-- /normal-sortables -->
    		</div>
    		<!-- /post-body-content -->
        </div>
    </div>
</div>
