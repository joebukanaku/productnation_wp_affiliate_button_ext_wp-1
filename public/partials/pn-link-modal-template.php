<?php
/**
*   {Tag} PN Custom
*   Add a comment to this line
*   - Link Modal Popup template
*/
?>
<div id='pn-link-modal' class='zoom-anim-dialog mfp-hide'>
    <img class="logo" src="<?php echo plugin_dir_url(__FILE__). "../img/productnation.png" ?>" />
    <h3 class="pn-link-modal-title">
        <?php
            echo $this->text['pn_link_modal_text'];
        ?>
    </h3>
    <div id="pn-link-btn-placement">
        <a id="pn-app-yes" class="fasc-button fasc-size-medium fasc-type-flat pn-link-btn"> Yes </a>
        <a id="pn-app-no" class="fasc-button fasc-size-medium fasc-type-flat pn-link-btn"> No </a>
    </div>
</div>
