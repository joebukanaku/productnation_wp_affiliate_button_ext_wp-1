/*
*	{Tag} PN Custom
*   - Remove button class
*/

(function( $ ) {

	'use strict';

	$(function(){
	  $('.fasc-type-normal-link')
	  	.removeClass('fasc-button fasc-size-xsmall fasc-size-small fasc-size-medium fasc-size-large fasc-size-xlarge ')
	  	.removeAttr('style');
	});
})( jQuery );
